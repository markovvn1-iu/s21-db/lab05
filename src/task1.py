#!/usr/bin/env python3


import time
import os
import logging
import psycopg2


INSERT_BLOCKS = 10


logging.basicConfig(format='%(asctime)s [%(name)s] [%(levelname)s] %(message)s', level=logging.INFO)

logger = logging.getLogger(__name__)


def create_btree_index(con, table_name):
    cur = con.cursor()
    cur.execute(f"CREATE INDEX myindex_btree ON {table_name} USING btree (age ASC NULLS LAST)")
    logger.info("Index (method: btree) no table %s created successfully", table_name)

def create_hash_index(con, table_name):
    cur = con.cursor()
    cur.execute(f"CREATE INDEX myindex_hash ON {table_name} USING hash (age)")
    logger.info("Index (method: hash) no table %s created successfully", table_name)

def measure_select1(con, table_name):
    cur = con.cursor()
    cur.execute(f"EXPLAIN ANALYZE SELECT * FROM {table_name} WHERE age > 21 AND age <= 46")
    return [i[0] for i in cur.fetchall()]

def measure_select2(con, table_name):
    cur = con.cursor()
    cur.execute(f"EXPLAIN ANALYZE SELECT * FROM {table_name} WHERE age = 36")
    return [i[0] for i in cur.fetchall()]

def process(con):
    create_btree_index(con, "customer_4del_2")
    create_hash_index(con, "customer_4del_3")

    with open("ex1_report.txt", "w") as f:
        f.write("########### QUERY 1 (age > 21 AND age <= 46) ###########\n")
        f.write("OUTPUT (no index):\n")
        f.write("\n".join(measure_select1(con, "customer_4del_1")))
        f.write("\n\nOUTPUT (btree index):\n")
        f.write("\n".join(measure_select1(con, "customer_4del_2")))
        f.write("\n\nOUTPUT (hash index):\n")
        f.write("\n".join(measure_select1(con, "customer_4del_3")))

        f.write("\n\n\n\n########### QUERY 2 (age == 36) ###########\n")
        f.write("OUTPUT (no index):\n")
        f.write("\n".join(measure_select2(con, "customer_4del_1")))
        f.write("\n\nOUTPUT (btree index):\n")
        f.write("\n".join(measure_select2(con, "customer_4del_2")))
        f.write("\n\nOUTPUT (hash index):\n")
        f.write("\n".join(measure_select2(con, "customer_4del_3")))

    logger.info("Report has created")

def main():
    for i in range(20):
        try:
            con = psycopg2.connect(
                database=os.getenv("POSTGRES_DB"),
                user=os.getenv("POSTGRES_USER"),
                password=os.getenv("POSTGRES_PASSWORD"),
                host="database", port="5432")
            logger.info("Connected to DB")
            process(con)
            con.commit()
            con.close()
            return
        except psycopg2.OperationalError as e:
            logger.warning("Error while connecting to DB: %s", str(e).strip())
            time.sleep(1)

    logger.critical("Failed to connect to DB")


if __name__ == "__main__":
    main()