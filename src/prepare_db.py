#!/usr/bin/env python3


import time
import os
import logging
import psycopg2
from psycopg2.extras import execute_values
from faker import Faker
import random


INSERT_BLOCKS = 10


logging.basicConfig(format='%(asctime)s [%(name)s] [%(levelname)s] %(message)s', level=logging.INFO)

logger = logging.getLogger(__name__)


def create_customer_table(con, table_name):
    cur = con.cursor()
    cur.execute(f"DROP TABLE IF EXISTS {table_name}")
    cur.execute(f"""CREATE TABLE {table_name}
        (id SERIAL PRIMARY KEY NOT NULL,
        name TEXT NOT NULL,
        address TEXT NOT NULL,
        age INT NOT NULL,
        review TEXT)""")
    logger.info("Table %s created successfully", table_name)


def insert_fake_rows(con, table_name, count):
    cur = con.cursor()

    fake = Faker()

    for i in range(INSERT_BLOCKS):
        ccount = count * (i + 1) // INSERT_BLOCKS - count * i // INSERT_BLOCKS

        logger.info("Progress: %d / %d", i, INSERT_BLOCKS)
        data = [(fake.name(), fake.address(), random.randint(0, 99), fake.text()) for i in range(ccount)]
        execute_values(cur, f"INSERT INTO {table_name} (name,address,age,review) VALUES %s", data)
        con.commit()
        
    logger.info("%d rows inserted into table %s", count, table_name)


def copy_table(con, f, t):
    cur = con.cursor()
    cur.execute(f"INSERT INTO {t} SELECT * FROM {f}")
    logger.info("Data from table %s copped to table %s", f, t)


def process(con):
    create_customer_table(con, "customer_4del_1")
    create_customer_table(con, "customer_4del_2")
    create_customer_table(con, "customer_4del_3")
    insert_fake_rows(con, "customer_4del_1", 100000)
    copy_table(con, "customer_4del_1", "customer_4del_2")
    copy_table(con, "customer_4del_1", "customer_4del_3")


def main():
    for i in range(20):
        try:
            con = psycopg2.connect(
                database=os.getenv("POSTGRES_DB"),
                user=os.getenv("POSTGRES_USER"),
                password=os.getenv("POSTGRES_PASSWORD"),
                host="database", port="5432")
            logger.info("Connected to DB")
            process(con)
            con.commit()
            con.close()
            return
        except psycopg2.OperationalError as e:
            logger.warning("Error while connecting to DB: %s", str(e).strip())
            time.sleep(1)

    logger.critical("Failed to connect to DB")


if __name__ == "__main__":
    main()