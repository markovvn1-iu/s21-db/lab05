# Lab 05

### Task 1

See `src/ex1_report.txt`.
Notice that:
1. In first query table with hash index work slower than table without index (because hash is bad in queries like A > B).
2. Btree always faster than hash indexes. Probably it is because I compare numbers (should try it with strings).
3. In general hashes speedup these queries a lot.

![image of generated data](./images/img1.png "Generated data")

To run use:
```
docker-compose build
docker-compose up
```